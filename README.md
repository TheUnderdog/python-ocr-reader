#Comment to stop GitLab deleting this stuff

# Python OCR Reader

A basic but buggy OCR reader tool. This is an offline OCR reader tool that doesn't make use of unethical Google libraries.

**Known Bugs**

1. English text when read omits all whitespace. This appears to be an issue with the 'tr' OCR library used.

To fix the bug where it doesn't read whitespace, enter:

* pip3 show tr

Which will reveal the installation directory of the TR library.

Navigate to the 'tr' folder under that installation directory, open

* char_table.txt

And anywhere in the character recognition list, include the ' ' character. You can also add any other characters here, although it likely slows down image recognition.

2. OCR tends to confuse lowercase 'e' with 'c'

**Library Requirements**

Must use Python 3 (call the script using python3).

* pyscreenshot

`pip3 install pyscreenshot`

* pymouse
 
`pip3 install pyuserinput`

* PIL [aka Python Imaging Library]

Because PIL is no longer maintained, it's advisible to use PIL's 'drop-in replacement', Pillow, which is actively maintained:

`pip3 install Pillow`

Also consider installing ffmpeg for Python:

`pip3 install python-ffmpeg`

* tr

This is a third-party library and requires a git import to work. See: https://www.kaggle.com/anycode/offline-ocr

To install it for Python3:

`pip3 install git+https://github.com/myhub/tr`

* pyperclip

`pip3 install pyperclip`

* pydub

`pip3 install pydub`


