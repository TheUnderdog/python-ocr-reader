#Version 1.0

import pyscreenshot as ImageGrab #pip install pyscreenshot
from PIL import Image, ImageDraw, ImageChops #pip install Pillow
from tr import *
import sys

CurrentDirectory = os.path.dirname(os.path.abspath(__file__))

G_MAX_HW = 2000

def IsOdd(IncomingNumber):
	return not (IncomingNumber % 2 == 0)

def CropImage(IncomingImage,LeftWidth,TopHeight,RightWidth,BottomHeight):
	return IncomingImage.crop((LeftWidth, TopHeight, RightWidth, BottomHeight))

def CropImageRelative(IncomingImage,LeftWidth,TopHeight,RightWidthRelative,BottomHeightRelative):
	RightWidth, BottomHeight = IncomingImage.size
	return CropImage(IncomingImage,LeftWidth,TopHeight,RightWidth+RightWidthRelative,BottomHeight+BottomHeightRelative)

def AutoTrimImage(IncomingImage,Buffer=0):
	IncomingImage = CropImageRelative(IncomingImage,0,0,0,-300)
	BackgroundColour = Image.new(IncomingImage.mode, IncomingImage.size, IncomingImage.getpixel((0,0)))
	TempDifference = ImageChops.difference(IncomingImage, BackgroundColour)
	TempDifference = ImageChops.add(TempDifference, TempDifference, 1.0, -100)
	BoundingBox = TempDifference.getbbox()
	
	if Buffer > 0:
		W, H = IncomingImage.size
		
		BB0 = BoundingBox[0] if BoundingBox[0] < Buffer else BoundingBox[0]-Buffer
		BB1 = BoundingBox[1] if BoundingBox[1] < Buffer else BoundingBox[1]-Buffer
		
		BB2 = BoundingBox[2] if BoundingBox[2] > (W-Buffer) else BoundingBox[2]+Buffer
		BB3 = BoundingBox[3] if BoundingBox[3] > (H-Buffer) else BoundingBox[3]+Buffer
		
		BoundingBox = (BB0,BB1,BB2,BB3)
	
	if IsOdd(BoundingBox[2] - BoundingBox[0]):
		BoundingBox = (BoundingBox[0],BoundingBox[1],BoundingBox[2]+1,BoundingBox[3])
		
	if IsOdd(BoundingBox[3] - BoundingBox[1]):
		BoundingBox = (BoundingBox[0],BoundingBox[1],BoundingBox[2],BoundingBox[3]+1)
	
	if BoundingBox:
		return IncomingImage.crop(BoundingBox)

	return IncomingImage

def AutoTrimImageFilename(IncomingFilename,Buffer=0):
	return AutoTrimImage(Image.open(IncomingFilename),Buffer)

def LoadImage(ImageFilename):

	LoadedImage = Image.open(ImageFilename)
	
	if LoadedImage.height > G_MAX_HW or LoadedImage.width > G_MAX_HW:
		
		while LoadedImage.height > G_MAX_HW or LoadedImage.width > G_MAX_HW:			
			TempScale = max(LoadedImage.height / G_MAX_HW, LoadedImage.width / G_MAX_HW)
		
			TempWidth = int(LoadedImage.width / TempScale + 0.5)
			TempHeight = int(LoadedImage.height / TempScale + 0.5)
			LoadedImage = LoadedImage.resize((TempWidth, TempHeight), Image.BICUBIC)
	
	return LoadedImage
	
def GreyscaleImage(LoadedImageFile):
	return LoadedImageFile.convert("L")

def ScanImageForText(IncomingImage):
	return run(GreyscaleImage(IncomingImage))

def ExtractTextFromImage(IncomingImage):
	Strang = ""
	ExtractedText = ScanImageForText(IncomingImage)
	for line in ExtractedText:
		Strang += line[1] + "\n"
		
	return Strang

def Screengrab(X1,Y1,X2,Y2):
	
	TempX = X1
	TempX2 = X2
	
	if X2 < X1:
		TempX = X2
		TempX2 = X1

	TempY = Y1
	TempY2 = Y2
	
	if Y2 < Y1:
		TempY = Y2
		TempY2 = Y1
	
	return ImageGrab.grab(bbox=(TempX,TempY,TempX2,TempY2))

	
def ExtractTextFromImageFilename(ImageFilename):
	return ExtractTextFromImage(LoadImage(ImageFilename))
