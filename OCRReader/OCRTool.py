import pyscreenshot as ImageGrab
#From pyuserinput
from pymouse import PyMouseEvent
from PIL import Image, ImageDraw
from tr import *
import sys
import pyperclip
from pydub import AudioSegment
from pydub.playback import play

CurrentDirectory = os.path.dirname(os.path.abspath(__file__))

G_MAX_HW = 2000

def LoadImage(ImageFilename):

	LoadedImage = Image.open(ImageFilename)
	
	if LoadedImage.height > G_MAX_HW or LoadedImage.width > G_MAX_HW:
		TempScale = max(LoadedImage.height / G_MAX_HW, LoadedImage.width / G_MAX_HW)
	
		TempWidth = int(LoadedImage.width / TempScale + 0.5)
		TempHeight = int(LoadedImage.height / TempScale + 0.5)
		LoadedImage = LoadedImage.resize((TempWidth, TempHeight), Image.BICUBIC)
	
	print(LoadedImage.width, LoadedImage.height)
	return LoadedImage
	
def GreyscaleImage(LoadedImageFile):
	return LoadedImageFile.convert("L");

def ScanImageForText(IncomingImage):
	return run(GreyscaleImage(IncomingImage))

def ExtractTextFromImage(IncomingImage):
	Strang = ""
	ExtractedText = ScanImageForText(IncomingImage)
	for line in ExtractedText:
		Strang += line[1] + "\n"
	
	return Strang

def Screengrab(X1,Y1,X2,Y2):
	
	TempX = X1
	TempX2 = X2
	
	if X2 < X1:
		TempX = X2
		TempX2 = X1

	TempY = Y1
	TempY2 = Y2
	
	if Y2 < Y1:
		TempY = Y2
		TempY2 = Y1
	
	return ImageGrab.grab(bbox=(TempX,TempY,TempX2,TempY2))
	
def ExtractTextFromScreen(X1,Y1,X2,Y2):
	Img = Screengrab(X1,Y1,X2,Y2)
	play(AudioSegment.from_mp3(CurrentDirectory+'/Sounds/tos_working.mp3'))
	return ExtractTextFromImage(Img)
	
def ExtractTextFromImageFilename(ImageFilename):
	Img = LoadImage(ImageFilename)
	return ExtractTextFromImage(Img)

class MouseXY(PyMouseEvent):
	def __init__(self):
		PyMouseEvent.__init__(self)
		self.Iter = 0
		self.X = None
		self.X2 = None
		self.Y = None
		self.Y2 = None

	def click(self, x, y, button, press):
		if button == 1:
			if press:
				if self.X is None:
					play(AudioSegment.from_mp3(CurrentDirectory+'/Sounds/tos_keypress4.mp3'))
					self.X = x
				else:
					play(AudioSegment.from_mp3(CurrentDirectory+'/Sounds/tos_keypress2.mp3'))
					self.X2 = x
				
				if self.Y is None:
					self.Y = y
				else:
					self.Y2 = y
				
				self.Iter += 1
				if self.Iter > 1:
									
					ExtractedText = ExtractTextFromScreen(self.X,self.Y,self.X2,self.Y2)
					pyperclip.copy(ExtractedText)
					play(AudioSegment.from_mp3(CurrentDirectory+'/Sounds/tos_verified.mp3'))
					exit()
		else:
			play(AudioSegment.from_mp3(CurrentDirectory+'/Sounds/tos_door_buzzer_short.mp3'))
			exit()

while True:
	
	#Second argument is the image filename
	if len(sys.argv) == 2:
		
		play(AudioSegment.from_mp3(CurrentDirectory+'/Sounds/tos_working.mp3'))
		ExtractedText = ExtractTextFromImageFilename(str(sys.argv[1]))
		pyperclip.copy(ExtractedText)
		play(AudioSegment.from_mp3(CurrentDirectory+'/Sounds/tos_verified.mp3'))
		
	else:
		
		C = MouseXY()
		play(AudioSegment.from_mp3(CurrentDirectory+'/Sounds/tos_hailing.mp3'))
		C.run()

	exit()
